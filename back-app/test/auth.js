const request = require('supertest');
const app = require('../app');

//==================== AUTH API test - Headers Missing ====================
describe('GET /dataUser Error - Header is Missing', function() {
    this.timeout(10000);
    it('Forbidden - /dataUser/id', function(done) {
        request(app)
            .get('/dataUser/id')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(403, {
                success: false,
                message: "Error - Headers missing"
            }, done);
    });
    it('Forbidden - /dataUser/name', function(done) {
        request(app)
            .get('/dataUser/name')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(403, {
                success: false,
                message: "Error - Headers missing"
            }, done);
    });
});

describe('GET /policies Error - Header is Missing', function() {
    this.timeout(10000);
    it('Forbidden - /policies', function(done) {
        request(app)
            .get('/policies')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(403, {
                success: false,
                message: "Error - Headers missing"
            }, done);
    });
    it('Forbidden - /policies/user', function(done) {
        request(app)
            .get('/policies/user')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(403, {
                success: false,
                message: "Error - Headers missing"
            }, done);
    });
});


//==================== AUTH API test - Incorrect Token ====================
describe('GET /dataUser Error - Incorrect Token', function() {
    this.timeout(10000);
    it('Forbidden - /dataUser/id', function(done) {
        request(app)
            .get('/dataUser/id')
            .set('Authorization', 'Bearer test')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(403, {
                success: false,
                message: "Error - Incorrect Token"
            }, done);
    });
    it('Forbidden - /dataUser/name', function(done) {
        request(app)
            .get('/dataUser/name')
            .set('Accept', 'application/json')
            .set('Authorization', 'Bearer test')
            .expect('Content-Type', /json/)
            .expect(403, {
                success: false,
                message: "Error - Incorrect Token"
            }, done);
    });
});

describe('GET /policies Error - Incorrect Token', function() {
    this.timeout(10000);
    it('Forbidden - /policies', function(done) {
        request(app)
            .get('/policies')
            .set('Accept', 'application/json')
            .set('Authorization', 'Bearer test')
            .expect('Content-Type', /json/)
            .expect(403, {
                success: false,
                message: "Error - Incorrect Token"
            }, done);
    });
    it('Forbidden - /policies/user', function(done) {
        request(app)
            .get('/policies/user')
            .set('Accept', 'application/json')
            .set('Authorization', 'Bearer test')
            .expect('Content-Type', /json/)
            .expect(403, {
                success: false,
                message: "Error - Incorrect Token"
            }, done);
    });
});