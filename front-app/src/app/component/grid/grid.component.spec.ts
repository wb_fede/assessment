import { GridComponent } from './grid.component';
import { BrastlewarkDataService } from '../../services/brastlewark-data.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable, from, throwError, of, empty} from 'rxjs';
import { TestBed, async } from '@angular/core/testing';
import { LoaderComponent } from '../loader/loader.component';

describe('GridComponent', () => {
    let component: GridComponent;
    const service = new BrastlewarkDataService(null);
    const activatedRoute = new ActivatedRoute();
    // activated TestBed
    beforeEach(
        async(() => {
          TestBed.configureTestingModule({
            declarations: [GridComponent, LoaderComponent]
          }).compileComponents();
        })
      );
    // init component
    beforeEach( () => {
        component = new GridComponent(service, activatedRoute);
    });

    it('NgOnInit - Service: Si hay un error en el servicio, debe mostrar el mensaje', () => {
        // spy
        spyOn( service, 'getBrastlewarkData').and.callFake(() => {
            return throwError(new Error('Fake error'));
          });

        component.ngOnInit();
        // check message and flags
        expect( component.loading).toBe(false);
        expect( component.error).toBe(true);
        expect( component.errorMessage).toContain('An error occurred');
    });


    it('NgOnInit - Excepcion route: debe mostrar mensaje ', () => {
        // activated route
        const route = ({ params: throwError(new Error('Fake error')) } as any) as ActivatedRoute;
        component = new GridComponent(service, route);
        // service
        const jsonFake = {
            'Brastlewark': []
        };
        // spy service
        spyOn( service, 'getBrastlewarkData').and.returnValue(of(jsonFake));

        // init
        component.ngOnInit();
        // check message and flags
        expect( component.loading).toBe(false);
        expect( component.error).toBe(true);
        expect( component.errorMessage).toContain('An error occurred');
    });


    it('NgOnInit - Sin datos: Si no hay datos, debe mostrar mensaje ', () => {
        // activated route
        const route = ({ params: of({}) } as any) as ActivatedRoute;
        component = new GridComponent(service, route);
        const jsonFake = {
            'Brastlewark': []
        };
        // spy service
        spyOn( service, 'getBrastlewarkData').and.returnValue(of(jsonFake));
        // init
        component.ngOnInit();
        // check flag
        expect( component.habitantscount).toBe(true);
    });
    // CHECK Filters
    const datatest = {
        'Brastlewark': [
            {
                'id': 0,
                'name': 'Habitant1',
                'thumbnail': 'http://www.publicdomainpictures.net/pictures/10000/nahled/thinking-monkey-11282237747K8xB.jpg',
                'age': 306,
                'weight': 39.065952,
                'height': 107.75835,
                'hair_color': 'Pink',
                'professions':
                    ['Metalworker'],
                'friends': ['Cogwitz Chillwidget']
            },
            {
                'id': 0,
                'name': 'Habitant2',
                'thumbnail': 'http://www.publicdomainpictures.net/pictures/10000/nahled/thinking-monkey-11282237747K8xB.jpg',
                'age': 306,
                'weight': 39.065952,
                'height': 107.75835,
                'hair_color': 'Pink',
                'professions':
                    ['Potter'],
                'friends': [ 'Tinadette Chillbuster']
            },
            {
                'id': 0,
                'name': 'Habitant3',
                'thumbnail': 'http://www.publicdomainpictures.net/pictures/10000/nahled/thinking-monkey-11282237747K8xB.jpg',
                'age': 306,
                'weight': 39.065952,
                'height': 107.75835,
                'hair_color': 'Pink',
                'professions':
                    ['Woodcarver'],
                'friends': ['Cogwitz Chillwidget']
            },
        ]
    };
    it('NgOnInit - Filter: filtro de Nombre, debe retornar 1 habitante', () => {
        // activated route
        const route = ({ params: of({prof: 'All', friend: 'All', name: 'Habitant1'}) } as any) as ActivatedRoute;
        component = new GridComponent(service, route);
        // service
         // spy
         spyOn( service, 'getBrastlewarkData').and.returnValue(of(datatest));
        // init
        component.ngOnInit();
        // check length
        expect( component.habitants.length).toBe(1);
    });
    it('NgOnInit - Filter: filtro de Profesión, debe retornar 1 habitante', () => {
        // activated route
        const route = ({ params: of({prof: 'Potter', friend: 'All', name: 'All'}) } as any) as ActivatedRoute;
        component = new GridComponent(service, route);
        // service
         // spy
         spyOn( service, 'getBrastlewarkData').and.returnValue(of(datatest));
        // init
        component.ngOnInit();
        // check length
        expect( component.habitants.length).toBe(1);
    });
    it('NgOnInit - Filter: filtro de Amigos, debe retornar 2 habitantes', () => {
        // activated route
        const route = ({ params: of({prof: 'All', friend: 'Cogwitz Chillwidget', name: 'All'}) } as any) as ActivatedRoute;
        component = new GridComponent(service, route);
        // service
         // spy
         spyOn( service, 'getBrastlewarkData').and.returnValue(of(datatest));
        // init
        component.ngOnInit();
        // check length
        expect( component.habitants.length).toBe(2);
    });
    it('NgOnInit - Filter: Todos los filtros con valor, debe retornar 0 habitantes', () => {
        // activated route
        const route = ({ params: of({prof: 'Potter', friend: 'Cogwitz Chillwidget', name: 'Habitant1'}) } as any) as ActivatedRoute;
        component = new GridComponent(service, route);
        // service
         // spy
         spyOn( service, 'getBrastlewarkData').and.returnValue(of(datatest));
        // init
        component.ngOnInit();
        // check length
        expect( component.habitants.length).toBe(0);
    });


});
