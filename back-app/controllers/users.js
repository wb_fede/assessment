const https = require('https');
var _ = require("underscore");
var jwt = require('jwt-simple');
var config = require('../config/config');

//Service
var responseService = require('../services/response');
var mockyService = require('../services/mocky');

// without certificate
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0'
    // Data
const type = 'clients';
// get list of users
exports.getUsersFilterID = function(req, res) {
    //check Id
    if (!req.query.id) {
        return responseService.createResponse(res, 403, false, "Error - ID is Missing");
    }

    //get data
    let options = { id: req.query.id };
    mockyService.getData(config.URL_CLIENT, options, type).then((data) => {
        const rdo = mockyService.handlerData(res, data, 'ID');
        return responseService.createResponse(res, rdo.status, rdo.success, rdo.message);
    }, (error) => {
        return responseService.createResponse(res, 500, false, 'Error Service');
    });

};


exports.getUsersFilterName = function(req, res) {

    //check Id
    if (!req.query.name) {
        return responseService.createResponse(res, 403, false, "Error - Name is Missing");
    }

    //get data
    let options = { name: req.query.name };
    mockyService.getData(config.URL_CLIENT, options, type).then((data) => {
        const rdo = mockyService.handlerData(res, data, 'Name');
        return responseService.createResponse(res, rdo.status, rdo.success, rdo.message);
    }, (error) => {
        return responseService.createResponse(res, 500, false, 'Error Service');
    });

};