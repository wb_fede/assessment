import { TestBed, ComponentFixture } from '@angular/core/testing';
import { LoaderComponent } from './loader.component';

describe('LoaderComponent', () => {
    let component: LoaderComponent;
    let fixture: ComponentFixture<LoaderComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [LoaderComponent]
        });

        fixture = TestBed.createComponent(LoaderComponent);
        component = fixture.componentInstance;
    });

    it('Debe de crearse el componente', () => {
        expect(component).toBeTruthy();
    });
});
