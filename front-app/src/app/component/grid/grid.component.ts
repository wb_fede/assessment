/*
Component: About Component
Description: Component of the Grid view.
Author: Devoto federico
*/

// Imports
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BrastlewarkDataService } from '../../services/brastlewark-data.service';
import { ActivatedRoute } from '@angular/router';
import * as JefNode from 'json-easy-filter';

// Decorator
@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})

export class GridComponent implements OnInit {

  // Variables
  ageImg;
  heightImg;
  widthImg;
  heroes: any[] = [];
  loading;
  habitants;
  error = false;
  errorMessage;
  parameters = null;
  habitantscount;

  constructor(private brastlewarkData: BrastlewarkDataService, private activatedRoute: ActivatedRoute) {
    // init variables
    this.loading = true;
    // count to show message
    this.habitantscount = false;
   }

  ngOnInit() {
    // execute service
    this.brastlewarkData.getBrastlewarkData().subscribe( data => {
      // success
        // filters
         this.activatedRoute.params.subscribe(params => {
           this.habitants = data['Brastlewark'];
           // Filter by name
        if(params.name !== 'All' && params.name)
        {
          this.habitants = new JefNode.JefNode(this.habitants).filter(function(node) {
                              if (node.has('name')) {
                                if(node.value.name.toLowerCase().search(params.name.toLowerCase()) >= 0){
                                      return node.value;
                                }}
                              });
          }
          // Filter by Professions
        if(params.prof !== 'All' && params.prof)
        {
          this.habitants = new JefNode.JefNode(this.habitants).filter(function(node) {
                              if (node.has('professions')) {
                                if( node.value.professions.indexOf(params.prof) >= 0){
                                      return node.value;
                                }}
                              });
          }
          // Filter by Friend 
          if( params.friend !== 'All' && params.friend)
          {
            this.habitants = new JefNode.JefNode(this.habitants).filter(function(node) {
                                if (node.has('friends')) {
                                  if(node.value.friends.indexOf(params.friend) >= 0){
                                        return node.value;
                                  }}
                                });
            }
            // show message
            if (this.habitants.length === 0 || this.habitants === undefined){
              this.habitantscount = true;
            } else {
              this.habitantscount = false;
            }
      }, responseError => {
        // error
        this.loading = false;
        this.error = true;
        this.errorMessage = 'An error occurred getting information - Please refresh';
      }
    );
      this.loading = false;
    }, responseError => {
      // error
      this.loading = false;
      this.error = true;
      this.errorMessage = 'An error occurred getting information - Please refresh';
    });
  }

}
