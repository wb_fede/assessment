/*
Component: About Component
Description: Filter. Use this component to filter data to search Component
Author: Devoto federico
*/

// Import
import { Pipe, PipeTransform } from '@angular/core';
import * as concat from 'unique-concat';

// Decorator
@Pipe({name: 'filter'})
export class FilterPipe implements PipeTransform {
    transform(value: any, attr: string): any {
        // any value
        if ( value == null) {
            return '';
        }
        // formated
        const objectJSON = JSON.parse(value);
        let profArray: string[];
        switch (attr) {
            case 'professions':
            // get professions list
            let elementFilter = objectJSON.map(function(p) { return p['professions']});
            // array to concat
            profArray = [];
            if(elementFilter[0] !== undefined){
                elementFilter.forEach(element => {
                    profArray = concat(profArray, element);
                    });
            }
                break;
            case 'friends':
            // get friends list
             elementFilter = objectJSON.map(function(p) { return p['friends']});
            // array to concat
            profArray = [];
            if (elementFilter[0] !== undefined) {
                elementFilter.forEach(element => {
                    profArray = concat(profArray, element);
                    });
            }
                break;
            default:
                break;
        }
        return profArray;
    }
}

