/*
Component: About Component
Description: Component of the Search view
Author: Devoto federico
*/

// Import
import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { BrastlewarkDataService } from '../../services/brastlewark-data.service';

// Decorator
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})


export class SearchComponent implements OnInit {
  // Variables
  filtersData;
  profFilter = 'All';
  friendFilter = 'All';

  constructor(private brastlewarkData: BrastlewarkDataService, private router: Router, private routerActivated: ActivatedRoute) {
  }

  ngOnInit() {
    // execute service
    this.brastlewarkData.getBrastlewarkData().subscribe( data => {
      // success
      this.routerActivated.params.subscribe(params => {
        // Params of URL
        if (params.prof) {
          this.profFilter = params.prof;
        }
        if (params.friend) {
          this.friendFilter = params.friend;
        }
      });
      this.filtersData = data['Brastlewark'];
    }, error => {
      this.filtersData = null;
    });
  }
  // Function to redirect
  filter(name) {
    if ( name == null || name.value === '') {
      this.router.navigate(['/home', 'All', this.profFilter, this.friendFilter]);
    } else {
      this.router.navigate(['/home', name.value, this.profFilter, this.friendFilter]);
    }
  }
  // Detect Change of value
  changeProfession(value) {
    this.profFilter = value.value;
  }
  changeFriend(value) {
    this.friendFilter = value.value;
  }
}
