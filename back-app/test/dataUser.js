const request = require('supertest');
const app = require('../app');
var token = '';
const idUser = 'a0ece5db-cd14-4f21-812f-966633e7be86';

before(
    function() {
        var user = request(app)
            .get('/api')
            .query({ id: idUser })
            .end(function(err, res) {
                // user1 will manage its own cookies
                // res.redirects contains an Array of redirects
                token = 'Bearer ' + res.body.message;
            });
    }
);
// DATA USER ID
describe('GET /dataUser/ID', function() {
    this.timeout(10000);
    it('Not ID - Error - ID is Missing', function(done) {
        request(app)
            .get('/dataUser/id')
            .set('Accept', 'application/json')
            .set('Authorization', token)
            .expect('Content-Type', /json/)
            .expect(403)
            .expect(function(res) {
                if (!('success' in res.body)) throw new Error("Error success key")
                if (!('message' in res.body)) throw new Error("Error message key")
            })
            .end(done)
    });

    it('Incorrect ID - Error - Incorrect ID', function(done) {
        request(app)
            .get('/dataUser/id')
            .set('Accept', 'application/json')
            .query({ id: 'TEST' })
            .set('Authorization', token)
            .expect('Content-Type', /json/)
            .expect(404)
            .expect(function(res) {
                if (!('success' in res.body)) throw new Error("Error success key")
                if (!('message' in res.body)) throw new Error("Error message key")
            })
            .end(done)
    });

    it('Correct ID - Get Data', function(done) {
        request(app)
            .get('/dataUser/id')
            .set('Accept', 'application/json')
            .query({ id: idUser })
            .set('Authorization', token)
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function(res) {
                if (!('message' in res.body)) throw new Error("Missing message key")
                if (!('success' in res.body)) throw new Error("Missing success key")
                if (!('name' in res.body.message[0])) throw new Error("Missing name key")
                if (!('email' in res.body.message[0])) throw new Error("Missing email key")
                if (!('role' in res.body.message[0])) throw new Error("Missing role key")
            })
            .end(done)
    });
});

//DATA USER NAME
describe('GET /dataUser/name', function() {
    this.timeout(10000);
    it('Not Name - Error - Name is Missing"', function(done) {
        request(app)
            .get('/dataUser/name')
            .set('Accept', 'application/json')
            .set('Authorization', token)
            .expect('Content-Type', /json/)
            .expect(403)
            .expect(function(res) {
                if (!('success' in res.body)) throw new Error("Error success key")
                if (!('message' in res.body)) throw new Error("Error message key")
            })
            .end(done)
    });

    it('Incorrect Name - Error - User not found', function(done) {
        request(app)
            .get('/dataUser/name')
            .set('Accept', 'application/json')
            .query({ name: 'TEST' })
            .set('Authorization', token)
            .expect('Content-Type', /json/)
            .expect(404)
            .expect(function(res) {
                if (!('success' in res.body)) throw new Error("Error success key")
                if (!('message' in res.body)) throw new Error("Error message key")
            })
            .end(done)
    });

    it('Correct Name - Get Data', function(done) {
        request(app)
            .get('/dataUser/name')
            .set('Accept', 'application/json')
            .query({ name: 'Britney' })
            .set('Authorization', token)
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function(res) {
                if (!('message' in res.body)) throw new Error("Missing message key")
                if (!('success' in res.body)) throw new Error("Missing success key")
                if (!('name' in res.body.message[0])) throw new Error("Missing name key")
                if (!('email' in res.body.message[0])) throw new Error("Missing email key")
                if (!('role' in res.body.message[0])) throw new Error("Missing role key")
            })
            .end(done)
    });
});