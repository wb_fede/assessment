var express = require('express');
var bodyParser = require('body-parser');
var methodOverride = require("method-override");
var winston = require('winston'),
    expressWinston = require('express-winston');
//APP
var app = express();
// APP Use
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
//Config Html
app.use(express.static(__dirname + '/node_modules/jquery/dist'));
app.use(express.static(__dirname + '/node_modules/bootstrap/dist'));
app.use(express.static(__dirname + '/public'));
// Log request
app.use(expressWinston.logger({
    transports: [
        new winston.transports.File({ filename: 'logs/app.log', level: 'info' }),
    ],
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json()
    ),
    meta: true,
    msg: function(req, res) { return `${res.statusCode} - ${req.method}` },
    expressFormat: true,
    colorize: false,
    ignoreRoute: function(req, res) { return false; }
}));

//routes
app.use(require('./routes/routes'));

// Log Errors
app.use(expressWinston.errorLogger({
    transports: [
        new winston.transports.Console({}),
        // new winston.transports.Console({level: 'warn'}), // also works
        // new winston.transports.Console({level: 'error'}), // DOES NOT work (reproduces bug)
    ],
    msg: '{{err.message}}',
    level: function() {
        return 'warn';
    }
}));

// Start server
var server = app.listen(3000);

//export
module.exports = server;


//Controller
/*var UserController = require('./controllers/users');
var AutenticationController = require('./controllers/auth');
var PoliciesController = require('./controllers/policies');*/
//router
//var router = express.Router();
//Logs
/*
var winston = require('winston');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;
var msg;
const logger = winston.createLogger({
    format: combine(
        timestamp(),
        format.json()

    ),
    transports: [
        new winston.transports.File({ filename: 'logs/app.log', level: 'info' }),
        new winston.transports.File({ filename: 'logs/error.log', level: 'error' })
    ]
});

// Index
/*router.get('/', function(req, res) {
    app.use(express.static(__dirname + '/node_modules/jquery/dist'));
    app.use(express.static(__dirname + '/node_modules/bootstrap/dist'));
    app.use(express.static(__dirname + '/public'));
    res.sendFile('index.html', {
        root: './public'
    });
});*/

//Autentication -> receive a token
/*
router.get('/api', function(req, res) {
    logger.log({
        level: 'info',
        message: {
            url: '/api',
            body: req.query,
            header: req.header
        }
    });
    AutenticationController.createToken(req, res)

});*/

// Data User by ID
/*router.get('/dataUser/id', middleware.ensureAuthenticated, function(req, res) {
    logger.log({
        level: 'info',
        message: {
            url: '/dataUser/id',
            body: req.query,
            header: req.header
        }
    });
    if (!req.query.id) {
        msg = "Error - ID is Missing";
        logger.log({
            level: 'info',
            message: msg
        });
        return res
            .status(403)
            .send({ success: false, message: msg });
    }

    //get user data
    UserController.getUsersFilterID(req, res, req.query.id).then(function(data) {
        //get count
        var count = Object.keys(data).length;
        if (count > 0) {
            msg = data;
            logger.log({
                level: 'info',
                message: msg
            });
            return res
                .status(200)
                .send({ success: true, message: data });
        } else {
            //user not found
            msg = 'Error - Incorrect ID';
            logger.log({
                level: 'info',
                message: msg
            });
            return res
                .status(404)
                .send({ success: false, message: msg });

        }
    }, function() {
        msg = 'Error Service';
        logger.log({
            level: 'error',
            message: msg
        });
        return res
            .status(500)
            .send({ success: false, message: msg });
    })
});
*/
// Data User by ID
/*
router.get('/dataUser/name', middleware.ensureAuthenticated, function(req, res) {
    logger.log({
        level: 'info',
        message: {
            url: '/dataUser/name',
            body: req.query,
            header: req.header
        }
    });
    if (!req.query.name) {
        msg = "Error - Name is Missing";
        logger.log({
            level: 'info',
            message: msg
        });
        return res
            .status(403)
            .send({ success: false, message: msg });
    }

    //get user data
    UserController.getUsersFilterName(req, res, req.query.name).then(function(data) {
        //get count
        var count = Object.keys(data).length;
        if (count > 0) {
            msg = data;
            logger.log({
                level: 'info',
                message: msg
            });
            return res
                .status(200)
                .send({ success: false, message: data });
        } else {
            //user not found
            msg = 'User not found';
            logger.log({
                level: 'info',
                message: msg
            });
            return res
                .status(404)
                .send({ success: false, message: msg });

        }
    }, function() {
        msg = 'Service error';
        logger.log({
            level: 'error',
            message: msg
        });
        return res
            .status(500)
            .send({ success: false, message: msg });

    })
});
*/
// User list policies
/*
router.get('/policies', middleware.ensureAuthenticated, function(req, res) {
    logger.log({
        level: 'info',
        message: {
            url: '/policies',
            body: req.query,
            header: req.header
        }
    });
    if (!req.query.id) {
        msg = "Error - User ID is Missing";
        logger.log({
            level: 'info',
            message: msg
        });
        return res
            .status(403)
            .send({ success: false, message: msg });
    }
    AutenticationController.validateRole(req, res).then(function(data) {
        PoliciesController.getPoliciesUser(req, res, req.query.id).then(function(data) {
            var count = Object.keys(data).length;
            if (count > 0) {
                msg = data;
                logger.log({
                    level: 'info',
                    message: msg
                });
                return res
                    .status(200)
                    .send({ success: true, policies: msg });
            } else {
                //user not found
                msg = "Error - User not found";
                logger.log({
                    level: 'info',
                    message: msg
                });
                return res
                    .status(404)
                    .send({ success: false, message: msg });

            }
        }, function() {
            msg = "Error - Service";
            logger.log({
                level: 'error',
                message: msg
            });
            return res
                .status(500)
                .send({ success: false, message: msg });
        });
    }, function() {
        msg = "Error - Acceso denegado";
        logger.log({
            level: 'info',
            message: msg
        });
        return res
            .status(403)
            .send({ success: false, message: msg });
    });
});
*/
// User user by policies
/*
router.get('/policies/user', middleware.ensureAuthenticated, function(req, res) {
    logger.log({
        level: 'info',
        message: {
            url: '/policies/user',
            body: req.query,
            header: req.header
        }
    });
    if (!req.query.id) {
        msg = "Error - Policy ID is Missing";
        logger.log({
            level: 'info',
            message: msg
        });
        return res
            .status(403)
            .send({ success: false, message: msg });
    }
    AutenticationController.validateRole(req, res).then(function(data) {
        PoliciesController.getUserByPolicie(req, res, req.query.id).then(function(data) {
            var count = Object.keys(data).length;
            if (count > 0) {

                UserController.getUsersFilterID(req, res, data[0].clientId).then(function(data) {
                    msg = data;
                    logger.log({
                        level: 'info',
                        message: msg
                    });
                    return res
                        .status(200)
                        .send({ success: true, user: msg });
                }, function() {
                    //user not found
                    msg = 'User not found';
                    logger.log({
                        level: 'info',
                        message: msg
                    });
                    return res
                        .status(404)
                        .send({ success: false, message: msg });

                });
            } else {
                //policies not found
                msg = 'Policies not found';
                logger.log({
                    level: 'info',
                    message: msg
                });
                return res
                    .status(404)
                    .send({ success: false, message: msg });

            }
        }, function() {
            msg = 'service error';
            logger.log({
                level: 'error',
                message: msg
            });
            return res
                .status(500)
                .send({ success: false, message: msg });
        });
    }, function() {
        msg = 'Acceso Denegado';
        logger.log({
            level: 'info',
            message: msg
        });
        return res
            .status(403)
            .send({ success: false, message: msg });
    });
});
*/