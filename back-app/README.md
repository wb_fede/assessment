# Backend App

Descripción del ejercicio disponible en

https://www.dropbox.com/sh/gfqisikrhuslbu0/AADUE52toTZKPjM6AHmNrkKMa?dl=0

## Getting Started

### Prerequisites

	- Node Js v8.11.2

### Installing

	1 - Clonar el repositorio.
	2 - Ejecutar 'npm i' para instalar las dependencias (Node_modules).
	3 - Ejecutar 'node app' para iniciar el servidor.

## Documentation

### PDF File

Dentro de la carpeta principal se encuentra un documento llamado BackApp.pdf que contiene la documentación de la API Rest

### HTML

En la raíz también hay un archivo HTML que contiene documentación y formas de ejecutar la API. Se puede visualizar ingresando en http://localhost:3000/ 

## Running the tests

### API Rest

La Api contiene las siguientes pruebas:

	GET /api
    √ Not ID - Error - ID is Missing
    √ Incorrect ID - Error - Incorrect ID (2168ms)
    √ Correct ID - Get correct data (1692ms)

  GET /dataUser Error - Header is Missing
    √ Forbidden - /dataUser/id
    √ Forbidden - /dataUser/name

  GET /policies Error - Header is Missing
    √ Forbidden - /policies
    √ Forbidden - /policies/user

  GET /dataUser Error - Incorrect Token
    √ Forbidden - /dataUser/id
    √ Forbidden - /dataUser/name

  GET /policies Error - Incorrect Token
    √ Forbidden - /policies
    √ Forbidden - /policies/user

  GET /dataUser/ID
    √ Not ID - Error - ID is Missing
    √ Incorrect ID - Error - Incorrect ID (1858ms)
    √ Correct ID - Get Data (1744ms)

  GET /dataUser/name
    √ Not Name - Error - Name is Missing"
    √ Incorrect Name - Error - User not found (1706ms)
    √ Correct Name - Get Data (1471ms)

  GET /policies
    √ Not Id - Error - User ID is Missing
    √ Not User - Error - User not found (2142ms)
    √ Forbidden Access - Error - Acceso denegado
    √ Correct ID - Get Data (2046ms)

  GET /policies/user
    √ Not Id - Error -  ID is Missing
    √ Policies not found (1994ms)
    √ Forbidden Access - Error - Acceso denegado

Para realizar las pruebas, se debe ejecutar el comando 'mocha test'.

Además se puede probar la aplicación mediante POSTMAN, usando la siguiente colección:

https://www.getpostman.com/collections/12d430247f751cd4c179

## Built With

	- Node Js v8.11.2
	- Express 4.16.0


## Authors

Devoto Federico 

