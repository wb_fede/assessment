import { TestBed, inject } from '@angular/core/testing';

import { BrastlewarkDataService } from './brastlewark-data.service';

describe('BrastlewarkDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{provide: BrastlewarkDataService , useValue: new BrastlewarkDataService(null)}]
    });
  });

  it('Debe ser Creado', inject([BrastlewarkDataService], (service: BrastlewarkDataService) => {
    expect(service).toBeTruthy();
  }));
});
