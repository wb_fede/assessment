import { FilterPipe } from './filter.pipe';

describe('FilterPipe', () => {
    const component = new FilterPipe();
    it('Transform: Si recibe Null, debe devolver vacío', () => {
        const rdo = component.transform(null, null);
        expect(rdo).toBe('');
    });

    it('Transform: filtro profesión - Empty values', () => {
        const datatest = '[{ "name":"John", "age":30, "city":"New York"}]';
        const rdo = component.transform(datatest, 'professions');
        expect(rdo).toEqual([]);
    });

    it('Transform: filtro profesión - 1 valor', () => {
        const datatest = '[{"professions":["Metalworker"]}]';
        const rdo = component.transform(datatest, 'professions');
        expect(rdo[0]).toEqual('Metalworker');
    });

    it('Transform: filtro friends - Empty values', () => {
        const datatest = '[{ "name":"John", "age":30, "city":"New York"}]';
        const rdo = component.transform(datatest, 'friends');
        expect(rdo).toEqual([]);
    });

    it('Transform: filtro friends - 1 valor', () => {
        const datatest = '[{"friends":["Metalworker"]}]';
        const rdo = component.transform(datatest, 'friends');
        expect(rdo[0]).toEqual('Metalworker');
    });
});
