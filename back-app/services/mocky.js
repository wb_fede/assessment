const https = require('https');
const _ = require("underscore");


exports.getData = function(url, options, type) {
    return new Promise(function(resolve, reject) {
        let data = '';
        https.get(url, (resp) => {
            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                data += chunk;
            });
            resp.on('end', () => {
                data = JSON.parse(data);
                let filtered = _.where(data[type], options);
                resolve(filtered); //OK
            });
        }).on("error", (err) => {
            reject(); // FAIL
        });
    });
};

exports.handlerData = function(res, data, attr) {
    let response = {
        status: 200,
        message: '',
        success: true
    };
    //get count
    var count = Object.keys(data).length;
    if (count > 0) {
        response.message = data;
    } else {
        //user not found
        response.message = 'Error - Incorrect ' + attr;
        response.status = 404;
        response.success = false;
    }

    return response;

}