var express = require('express');
var router = express.Router();

//  Controllers
var appController = require('../controllers/app');
var userController = require('../controllers/users');
var policiesController = require('../controllers/policies');
//Service
var authenthicationService = require('../services/auth');
/* Routes */
// Index
router.get('/', appController.index);

// Auth Controller
router.get('/api', authenthicationService.createToken);

// User Controller
router.get('/dataUser/id', authenthicationService.ensureAuthenticated, userController.getUsersFilterID);
router.get('/dataUser/name', authenthicationService.ensureAuthenticated, userController.getUsersFilterName);

// Policies Controller
router.get('/policies', [authenthicationService.ensureAuthenticated, authenthicationService.ensureAuthorization], policiesController.getPoliciesUser);
router.get('/policies/user', [authenthicationService.ensureAuthenticated, authenthicationService.ensureAuthorization], policiesController.getUserByPolicie);

//Export
module.exports = router;