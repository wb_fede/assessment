var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('../config/config');
//const https = require('https');
//controller
var userController = require('../controllers/users');

//Service
var responseService = require('../services/response');
var mockyService = require('../services/mocky');

// without certificate
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
const type = 'clients';
//Authenticate and Authorization 
exports.createToken = function(req, res) {
    //need id
    if (!req.query.id) {
        return responseService.createResponse(res, 404, false, "Error - ID is Missing");
    }
    //Get data Filtered by id
    let options = { id: req.query.id };
    mockyService.getData(config.URL_CLIENT, options, type).then((data) => {
        //get count
        let count = Object.keys(data).length;
        if (count > 0) {
            let role = data[0].role;
            let payloadSub = req.query.id + ' ' + role;

            //generate token
            var payload = {
                sub: payloadSub,
                iat: moment().unix(),
                exp: moment().add(2, "hours").unix(),
            };
            // return the information including token as JSON
            return responseService.createResponse(res, 200, true, jwt.encode(payload, config.TOKEN_SECRET));

        } else { //Incorrect ID
            return responseService.createResponse(res, 404, false, "Error - Incorrect ID");
        }
    }, (error) => {
        console.log(error);
        return responseService.createResponse(res, 500, false, "Error with user service");
    });

};


//Authorization admin role
/*
exports.validateRole = function(req, res) {
    let promiseUserList = new Promise(function(resolve, reject) {
        try {
            let token = req.headers.authorization.split(" ")[1];
            let payload = jwt.decode(token, config.TOKEN_SECRET);
            let role = payload.sub.split(" ")[1];
            if (role != 'admin') {
                reject(); // FAIL
            }
            resolve(); // success
        } catch (e) {
            reject(); // FAIL
        }
    });

    return promiseUserList;

};*/

// Middleware
exports.ensureAuthenticated = function(req, res, next) {
    if (!req.headers.authorization) {
        return responseService.createResponse(res, 403, false, "Error - Headers missing");
    }

    try {
        var token = req.headers.authorization.split(" ")[1];
        var bearer = req.headers.authorization.split(" ")[0];
        var payload = jwt.decode(token, config.TOKEN_SECRET);

        if (bearer != 'Bearer') {
            return responseService.createResponse(res, 403, false, "Error - Incorrect Bearer");
        }
    } catch (error) {
        return responseService.createResponse(res, 403, false, "Error - Incorrect Token");
    }

    try {
        if (payload.exp <= moment().unix()) {
            return responseService.createResponse(res, 401, false, "Error - The token expires");
        }

        req.user = payload.sub;
        next();
    } catch (error) {
        return responseService.createResponse(res, 403, false, "Error - Incorrect Token");
    }
}

exports.ensureAuthorization = function(req, res, next) {
    try {
        let token = req.headers.authorization.split(" ")[1];
        let payload = jwt.decode(token, config.TOKEN_SECRET);
        let role = payload.sub.split(" ")[1];
        if (role != 'admin') {
            return responseService.createResponse(res, 403, false, "Acceso Denegado");
        }
        next();
    } catch (e) {
        return responseService.createResponse(res, 403, false, "Acceso Denegado");
    }
}