const request = require('supertest');
const app = require('../app');
var tokenUser = '';
var tokenAdmin = '';
const idAdmin = 'e8fd159b-57c4-4d36-9bd7-a59ca13057bb';
const idUser = 'a3b8d425-2b60-4ad7-becc-bedf2ef860bd';
const policies = '64cceef9-3a01-49ae-a23b-3761b604800b';

before(
    function() {
        var user = request(app)
            .get('/api')
            .query({ id: idUser })
            .end(function(err, res) {
                // user1 will manage its own cookies
                // res.redirects contains an Array of redirects
                tokenUser = 'Bearer ' + res.body.message;
            });

        var userAdmin = request(app)
            .get('/api')
            .query({ id: idAdmin })
            .end(function(err, res) {
                // user1 will manage its own cookies
                // res.redirects contains an Array of redirects
                tokenAdmin = 'Bearer ' + res.body.message;
            });
    }
);
// DATA policies
describe('GET /policies', function() {
    this.timeout(10000);
    it('Not Id - Error - User ID is Missing', function(done) {
        request(app)
            .get('/policies')
            .set('Accept', 'application/json')
            .set('Authorization', tokenAdmin)
            .expect('Content-Type', /json/)
            .expect(403)
            .expect(function(res) {
                if (!('success' in res.body)) throw new Error("Error success key")
                if (!('message' in res.body)) throw new Error("Error message key")
            })
            .end(done)
    });
    it('Not User - Error - User not found', function(done) {
        request(app)
            .get('/policies')
            .set('Accept', 'application/json')
            .set('Authorization', tokenAdmin)
            .query({ id: 'TEST' })
            .expect('Content-Type', /json/)
            .expect(404)
            .expect(function(res) {
                if (!('success' in res.body)) throw new Error("Error success key")
                if (!('message' in res.body)) throw new Error("Error message key")
            })
            .end(done)
    });
    it('Forbidden Access - Error - Acceso denegado', function(done) {
        request(app)
            .get('/policies')
            .set('Accept', 'application/json')
            .set('Authorization', tokenUser)
            .query({ id: 'TEST' })
            .expect('Content-Type', /json/)
            .expect(403)
            .expect(function(res) {
                if (!('success' in res.body)) throw new Error("Error success key")
                if (!('message' in res.body)) throw new Error("Error message key")
            })
            .end(done)
    });

    it('Correct ID - Get Data', function(done) {
        request(app)
            .get('/policies')
            .set('Accept', 'application/json')
            .query({ id: idAdmin })
            .set('Authorization', tokenAdmin)
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function(res) {
                if (!('success' in res.body)) throw new Error("Error success key")
                if (!('message' in res.body)) throw new Error("Error message key")
            })
            .end(done)
    });
});

// DATA USER Policies
describe('GET /policies/user', function() {
    this.timeout(10000);
    it('Not Id - Error -  ID is Missing', function(done) {
        request(app)
            .get('/policies/user')
            .set('Accept', 'application/json')
            .set('Authorization', tokenAdmin)
            .expect('Content-Type', /json/)
            .expect(403)
            .expect(function(res) {
                if (!('success' in res.body)) throw new Error("Error success key")
                if (!('message' in res.body)) throw new Error("Error message key")
            })
            .end(done)
    });

    it('Policies not found', function(done) {
        request(app)
            .get('/policies/user')
            .set('Accept', 'application/json')
            .set('Authorization', tokenAdmin)
            .query({ id: 'TEST' })
            .expect('Content-Type', /json/)
            .expect(404)
            .expect(function(res) {
                if (!('success' in res.body)) throw new Error("Error success key")
                if (!('message' in res.body)) throw new Error("Error message key")
            })
            .end(done)
    });
    it('Forbidden Access - Error - Acceso denegado', function(done) {
        request(app)
            .get('/policies/user')
            .set('Accept', 'application/json')
            .set('Authorization', tokenUser)
            .query({ id: 'TEST' })
            .expect('Content-Type', /json/)
            .expect(403)
            .expect(function(res) {
                if (!('success' in res.body)) throw new Error("Error success key")
                if (!('message' in res.body)) throw new Error("Error message key")
            })
            .end(done)
    });

});