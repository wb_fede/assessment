import { TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HomeComponent } from './home.component';
import { SearchComponent } from '../search/search.component';
import { GridComponent } from '../grid/grid.component';
import { FilterPipe } from 'src/app/pipes/filter.pipe';
import { LoaderComponent } from '../loader/loader.component';
import { Router, ActivatedRoute } from '@angular/router';
import { BrastlewarkDataService } from '../../services/brastlewark-data.service';
import { of } from 'rxjs';

describe('HomeComponent', () => {
    let component: HomeComponent;
    let fixture: ComponentFixture<HomeComponent>;
    const router = {
        navigate: jasmine.createSpy('navigate')
      };
      const service = new BrastlewarkDataService(null);
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [HomeComponent, SearchComponent, GridComponent, FilterPipe, LoaderComponent],
            providers: [
                { provide: Router, useValue: router },
                { provide: ActivatedRoute, useValue: { params: of({prof: 'All', friend: 'All', name: 'All'})}},
                {provide: BrastlewarkDataService, useValue: service},
            ],
            imports:[RouterTestingModule]
        });

        fixture = TestBed.createComponent(HomeComponent);
        component = fixture.componentInstance;
    });

    it('Debe de crearse el componente', () => {
        expect(component).toBeTruthy();
    });
});