const request = require('supertest');
const app = require('../app');

//==================== /Api API test ====================
describe('GET /api', function() {
    this.timeout(10000);
    it('Not ID - Error - ID is Missing', function(done) {
        request(app)
            .get('/api')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(404, {
                success: false,
                message: "Error - ID is Missing"
            }, done);
    });
    it('Incorrect ID - Error - Incorrect ID', function(done) {
        request(app)
            .get('/api')
            .query({ id: '1555' })
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(404, {
                success: false,
                message: "Error - Incorrect ID"
            }, done);
    });
    it('Correct ID - Get correct data', function(done) {
        request(app)
            .get('/api')
            .query({ id: 'a0ece5db-cd14-4f21-812f-966633e7be86' })
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function(res) {
                if (!('message' in res.body)) throw new Error("Missing message key")
                if (!('success' in res.body)) throw new Error("Missing success key")
            })
            .end(done)
    });
});