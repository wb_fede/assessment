// Angular
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { FooterComponent } from './component/footer/footer.component';
import { GridComponent } from './component/grid/grid.component';
import { SearchComponent } from './component/search/search.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { LoaderComponent } from './component/loader/loader.component';
import { AboutComponent } from './component/about/about.component';

// Services
import { BrastlewarkDataService } from './services/brastlewark-data.service';

// Routes
import { APP_ROUTING } from './app.routes';


// Pipes
import { FilterPipe } from './pipes/filter.pipe';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    FooterComponent,
    GridComponent,
    SearchComponent,
    LoaderComponent,
    FilterPipe,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    APP_ROUTING
  ],
  providers: [
    BrastlewarkDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
